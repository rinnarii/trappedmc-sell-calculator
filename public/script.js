function calculate() {
	function output(amount) {
		return "$" + amount.toFixed(2);
	}

	var copper = perBlock(90);
	var iron = perBlock(144);
	var gold = perBlock(270);
	
	document.getElementById("copperperblock").innerHTML = output(copper);
	document.getElementById("ironperblock").innerHTML = output(iron);
	document.getElementById("goldperblock").innerHTML = output(gold);

	var smelt = parseInt(document.getElementById("smelt").value);
	
	function doubleSmelt(amount) {
		return amount * (1 + smelt / 100);
	}

	var doubleSmeltCopper = doubleSmelt(copper);
	var doubleSmeltIron = doubleSmelt(iron);
	var doubleSmeltGold = doubleSmelt(gold);
	
	document.getElementById("coppersmelt").innerHTML = output(doubleSmeltCopper);
	document.getElementById("ironsmelt").innerHTML = output(doubleSmeltIron);
	document.getElementById("goldsmelt").innerHTML = output(doubleSmeltGold);
	
	function perStack(amount) {
		return amount * 64;
	}

	var perStackCopper = perStack(doubleSmeltCopper);
	var perStackIron = perStack(doubleSmeltIron);
	var perStackGold = perStack(doubleSmeltGold);
	
	document.getElementById("copperstack").innerHTML = output(perStackCopper);
	document.getElementById("ironstack").innerHTML = output(perStackIron);
	document.getElementById("goldstack").innerHTML = output(perStackGold);
	
	function perDub(amount) {
		return amount * 54;
	}
	
	var perDubCopper = perDub(perStackCopper);
	var perDubIron = perDub(perStackIron);
	var perDubGold = perDub(perStackGold);
	
	document.getElementById("copperdub").innerHTML = output(perDubCopper);
	document.getElementById("irondub").innerHTML = output(perDubIron);
	document.getElementById("golddub").innerHTML = output(perDubGold);
}
function perBlock(cost) {
	var prestige = parseInt(document.getElementById("prestige").value);
	var multiplier = 1;
	
	var sellBooster = document.getElementById("sellbooster");
	var sellSelection = sellBooster.options[sellBooster.selectedIndex].value;
	if (sellSelection == "15") multiplier *= 1.15;
	else if (sellSelection == "30") multiplier *= 1.3;
	
	var mallSell = document.getElementById("mallsell");
	var mallSelection = mallSell.options[mallSell.selectedIndex].value;
	if (mallSelection == "15") multiplier *= 0.85;
	
	if (prestige > 0) return multiplier * cost * (1 + (prestige + 2) / 100);
	else return multiplier * cost;
}